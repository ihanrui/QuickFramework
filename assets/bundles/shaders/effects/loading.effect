// Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.  

CCEffect %{
  techniques:
  - passes:
    - vert: vs
      frag: fs
      blendState:
        targets:
        - blend: true
      rasterizerState:
        cullMode: none
      properties:
        texture: { value: white }
        alphaThreshold: { value: 0.5 }
}%


CCProgram vs %{
  precision highp float;

  #include <cc-global>
  #include <cc-local>

  in vec3 a_position;
  in vec4 a_color;
  out vec4 v_color;

  #if USE_TEXTURE
  in vec2 a_uv0;
  out vec2 v_uv0;
  #endif

  void main () {
    vec4 pos = vec4(a_position, 1);

    #if CC_USE_MODEL
    pos = cc_matViewProj * cc_matWorld * pos;
    #else
    pos = cc_matViewProj * pos;
    #endif

    #if USE_TEXTURE
    v_uv0 = a_uv0;
    #endif

    v_color = a_color;

    gl_Position = pos;
  }
}%


CCProgram fs %{
  precision highp float;
  
  #include <alpha-test>
  #include <texture>
  #include <common>
  #include <cc-global>

  in vec4 v_color;

  #if USE_TEXTURE
  in vec2 v_uv0;
  uniform sampler2D texture;
  #endif

  //画圆，抗锯齿 参考https://mp.weixin.qq.com/s/QhKzmtpwiQgOzsGPcBHSJQ
  float circle(in vec2 uv, in float radius){
    return 1.0 - smoothstep(radius-0.01*radius, radius+0.01*radius, length(uv));
  }

  void main () {
    vec4 o = vec4(0, 0, 0, 0);

    #if USE_TEXTURE
      //CCTexture(texture, v_uv0, o);
      //让uv坐标居中
      vec2 uv = v_uv0 - 0.5;
      //小圆的半径
      float itemRadius = 0.05;
      //画小圆的数量 
      const float count = 12.0;
      //大圆的半径
      float radius = 0.35;
      //圆与圆之前的夹角
      float rotation = 0.0;
      float time = cc_time.x;
      float realItemRadius = 0.0;
      vec2 pos = vec2(0,0);
      for( float i = 0.0 ; i < count ; i++){
        //按总个数平分360度
        rotation = PI2 * i / count + time * PI2 / 4.0; 
        //计数在半径为radius的圆上各圆心位置
        pos = vec2(radius * cos(rotation),radius * sin(rotation));
        //按角度确定小圆的大小
        realItemRadius = itemRadius * (sin(rotation*0.45)+1.0)/3.0 + itemRadius/3.0; 
        //在各小圆圆心位置上画圆
        o.a += circle(uv - pos,realItemRadius);
        //颜色变化
        o.r += o.a * (sin(i/count + time + PI*6.0/3.0)+ 1.0)/2.0;
        o.g += o.a * (sin(i/count + time + PI*4.0/3.0)+1.0)/2.0;
        o.b += o.a * (sin(i/count + time + PI*2.0/3.0)+1.0)/2.0;
      }
    #endif

    o *= v_color;

    ALPHA_TEST(o);

    gl_FragColor = o;
  }
}%
