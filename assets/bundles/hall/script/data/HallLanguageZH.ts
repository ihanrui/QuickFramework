export let HALL_ZH = {
  language: cc.sys.LANGUAGE_CHINESE,
  data: {
    hall_view_game_name: [
      '游戏1',
      '游戏2',
      '坦克大战',
      '扩展Load接口示例',
      '网络示例',
      "瞄准线",
      "节点对象池",
      "Shader",
      "三消"
    ],
    hall_view_broadcast_content: '[系统广播] 恭喜大佬进入',
    hall_view_nogame_notice: '【{0}】未实现，更多功能，敬请期待!!!',
    test: "测试 : {0}-->{1}-->{2}-->{3}-->",
  }
}